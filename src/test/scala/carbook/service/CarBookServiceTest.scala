package carbook.service

import carbook.data.Car
import carbook.exceptions.{CarAlreadyExistException, CarIsNotExistException}
import carbook.storage.DataBaseImpl
import io.circe.Json
import org.scalatest.flatspec.AsyncFlatSpec

import scala.concurrent.Future

class CarBookServiceTest extends AsyncFlatSpec {

  implicit val dataBase: DataBaseImpl = DataBaseImpl("h2")

  val service: CarBookServiceImpl[Future] = CarBookServiceImpl.make[Future]

  behavior of "getCars"

  it should "return cars in json" in {

    val json = for {
      _ <- dataBase.createTable

      json <- service.getCars
    } yield json

    json.map(json => assert(json.isInstanceOf[Json]))

  }

  behavior of "getCarByNumber"

  it should "return json" in {

    val json = for {
      _ <- dataBase.createTable
      _ <- dataBase.insert(Car("aaa", "audi", "blue", 1974))
      json <- service.getCarByNumber("aaa")
    } yield json

    json.map(json => assert(json.isInstanceOf[Json]))

  }

  behavior of "getCarsByParameters"

  it should "return cars in json" in {
    val json = for {
      _ <- dataBase.createTable
      _ <- dataBase.insert(Car("aaa", "audi", "blue", 1974))
      json <- service.getCarsByParameters("audi", "blue", 1974)
    } yield json

    json.map(json => assert(json.isInstanceOf[Json]))
  }

  behavior of "insert"

  it should "insert car in db" in {
    val future = for {
      _ <- dataBase.createTable
      str <- service.insert(Car("aaa", "audi", "blue", 1974))
    } yield str

    future.map(str => assert(str == "inserting complete"))
  }

  it should "throw exception" in {
    val future = for {
      _ <- dataBase.createTable
      _ <- service.insert(Car("aaa", "audi", "blue", 1974))
      str <- service.insert(Car("aaa", "audi", "blue", 1974))
    } yield str

    recoverToSucceededIf[CarAlreadyExistException](future)
  }

  behavior of "delete"

  it should "delete car from db" in {
    val future = for {
      _ <- dataBase.createTable
      _ <- service.insert(Car("aaa", "audi", "blue", 1974))
      str <- service.delete("aaa")
    } yield str

    future.map(str => assert(str == "deleting complete"))
  }

  it should "throw exception" in {
    val future = for {
      _ <- dataBase.createTable
      str <- service.delete("aaa")
    } yield str

    recoverToSucceededIf[CarIsNotExistException](future)
  }

  behavior of "getStat"

  it should "return Json" in {
    val future = for {
      _ <- dataBase.createTable
      json <- service.getStat
    } yield json

    future.map(json => assert(json.isInstanceOf[Json]))
  }





}
