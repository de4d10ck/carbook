package carbook.service

import carbook.data.{Car, CarJson, Stat}
import carbook.exceptions.CarIsNotExistException
import carbook.storage.DataBase
import cats.Monad
import cats.implicits._
import io.circe.Json
import io.circe.syntax._

import scala.concurrent.ExecutionContext

class CarBookServiceImpl[F[_] : Monad : DataBase] private(ec: ExecutionContext) extends CarBookService[F] {

  override def getCars: F[Json] = DataBase[F].selectAll.map(seq => seq.map(convertCars).asJson)

  override def getCarByNumber(number: String): F[Json] = DataBase[F].find(number).map {
    case Some(car) => convertCars(car).asJson
    case None => CarIsNotExistException(number).getMessage.asJson
  }

  override def getCarsByParameters(model: String, color: String, prodDate: Int): F[Json] =
    DataBase[F].find(model, color, prodDate).map(seq => seq.map(convertCars).asJson)

  override def insert(car: Car): F[String] = DataBase[F].insert(car) flatMap {
    case 1 => Monad[F].pure("inserting complete")
  }

  override def delete(number: String): F[String] = DataBase[F].delete(number) flatMap {
    case 1 => Monad[F].pure("deleting complete")
  }

  override def getStat: F[Json] =
    for {
      count <- DataBase[F].countTable
      max <- DataBase[F].getDateMax
      min <- DataBase[F].getDateMin
    } yield Stat(count, min.headOption, max.headOption).asJson

  def convertCars(car: Car): CarJson = CarJson(car.number, car.model, car.color, car.prodYear)
}
object CarBookServiceImpl {
  def make[F[_] : Monad : DataBase](implicit ec: ExecutionContext) = new CarBookServiceImpl[F](ec)

  //  implicit def carBookServiceFuture(implicit ec: ExecutionContext): CarBookServiceFabric[Future] = make[Future]
}