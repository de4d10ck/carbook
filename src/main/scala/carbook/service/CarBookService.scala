package carbook.service

import carbook.data.Car
import io.circe.Json

trait CarBookService[F[_]] {

  def getCars: F[Json]

  def getCarByNumber(number: String): F[Json]

  def getCarsByParameters(model: String, color: String, prodDate: Int): F[Json]

  def insert(car: Car): F[String]

  def delete(number: String): F[String]

  def getStat: F[Json]

}

