package carbook

import carbook.storage.DataBaseImpl
import com.comcast.ip4s._
import cats.effect.{ExitCode, IO, IOApp}
import org.http4s.ember.server.EmberServerBuilder

import scala.concurrent.ExecutionContext

object MainHttp extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val ec: ExecutionContext = runtime.compute
    implicit val database: DataBaseImpl = DataBaseImpl("postgres")
    val route = new Routes()
    database.createTable
    EmberServerBuilder
      .default[IO]
      .withHost(ipv4"0.0.0.0")
      .withPort(port"8080")
      .withHttpApp(route.allRoutes)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }
}
