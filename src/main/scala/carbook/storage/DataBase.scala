package carbook.storage

import carbook.data.Car

import java.sql.Timestamp

trait DataBase[F[_]] {

  def createTable: F[Unit]

  def selectAll: F[Seq[Car]]

  def insert(car: Car): F[Int]

  def delete(number: String): F[Int]

  def find(number: String): F[Option[Car]]

  def find(model: String, color: String, prodDate: Int): F[Seq[Car]]

  def getDateMin: F[Seq[Timestamp]]

  def getDateMax: F[Seq[Timestamp]]

  def countTable: F[Int]

}

object DataBase {
  @inline def apply[F[_]](implicit instance: DataBase[F]): DataBase[F] = instance
}


