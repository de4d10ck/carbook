FROM openjdk:8
ADD /target/scala-2.13/CarBook-assembly-0.1.jar carbook.jar
ENTRYPOINT ["java", "-jar", "carbook.jar"]